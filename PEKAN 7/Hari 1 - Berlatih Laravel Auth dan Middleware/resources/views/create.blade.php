@extends('layout')

@section('content')
    <div class="container">
        <h2>Form Membuat Data Pemain Film Baru</h2>

        <!-- Form untuk membuat data pemain film baru -->
        <form method="POST" action="{{ url('/cast') }}">
            @csrf
            <div class="form-group">
                <label for="nama">Nama:</label>
                <input class="form-control" type="text" name="nama" required>
            </div>
            <div class="form-group">
                <label for="umur">Umur:</label>
                <input class="form-control" type="number" name="umur" required>
            </div>
            <div class="form-group">
                <label for="bio">Bio:</label>
                <input class="form-control" type="text" name="bio" required>
            </div>
            <button class="btn btn-primary" type="submit">Simpan</button>
        </form>
    </div>
@endsection
