@extends('layout')

@section('content')
    <div class="container">
        <h2>Detail Data Pemain Film</h2>

        <!-- Tampilkan detail data pemain film -->
        <p>ID: {{ $cast->id }}</p>
        <p>Nama: {{ $cast->nama }}</p>
        <!-- Add more details as needed -->
        <a href="{{ url('/cast') }}">Kembali ke List</a>
    </div>
@endsection
