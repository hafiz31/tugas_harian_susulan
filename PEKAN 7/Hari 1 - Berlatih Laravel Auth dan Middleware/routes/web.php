<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/cast');
});

Route::get('home', function () {
    return redirect('/cast');
});

Route::get('/cast', [CastController::class, 'index']);

Route::middleware(['auth'])->group(function () {
    Route::get('/cast/create', [CastController::class, 'create']);
    Route::post('/cast', [CastController::class, 'store']);
    Route::get('/cast/{cast_id}', [CastController::class, 'show']);
    Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
    Route::put('/cast/{cast_id}', [CastController::class, 'update']);
    Route::get('/cast/{cast_id}/destroy', [CastController::class, 'destroy']);
});

Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('/login', [LoginController::class, 'login']);
Route::get('/logout', [LoginController::class, 'logout']);
