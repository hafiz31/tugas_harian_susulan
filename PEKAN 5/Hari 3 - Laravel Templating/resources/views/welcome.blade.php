@extends('layout')

@section('content')
    <h1>SELAMAT DATANG {{ $data }}!</h1>
    <p>
        <b>Terima kasih telah bergabung di Website Kami. Media Belajar kita
            bersama!</b>
    </p>
@endsection
