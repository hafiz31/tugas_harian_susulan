@extends('layout')

@section('content')
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label for="firstname">First Name:</label>
        <br />
        <br />
        <input type="text" name="firstname" id="firstname" />
        <br />
        <br />
        <label for="lastname">Last Name :</label>
        <br />
        <br />
        <input type="text" name="lastname" id="lastname" />
        <br />
        <br />
        <label for="radio">Gender</label>
        <br />
        <br />
        <input type="radio" name="radio" id="radio" />Male <br />
        <input type="radio" name="radio" id="radio" />Female
        <br />
        <br />
        <label for="nationality">Nationality</label>
        <br />
        <br />
        <select name="select" id="select">
            <option value="1">Indonesia</option>
            <option value="2">England</option>
        </select>
        <br />
        <br />
        <label for="">Languange Spoken</label>
        <br />
        <br />
        <input type="checkbox" />Bahasa Indonesia
        <br />
        <input type="checkbox" />English
        <br />
        <input type="checkbox" />Other
        <br />
        <br />
        <label for="">Bio</label>
        <br />
        <textarea name="" id="" cols="30" rows="10"></textarea>
        <br />
        <button type="submit">Sign Up</button>
    </form>
@endsection
