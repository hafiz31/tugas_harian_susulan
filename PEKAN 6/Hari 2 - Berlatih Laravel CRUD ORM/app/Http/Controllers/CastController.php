<?php

namespace App\Http\Controllers;

use App\Models\Cast;
use Illuminate\Http\Request;

class CastController extends Controller
{
    public function index()
    {
        $casts = Cast::all();
        return view('index', compact('casts'));
    }

    public function create()
    {
        return view('create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|max:45',
        ]);

        Cast::create($request->all());

        return redirect('/cast')->with('success', 'Data pemain film berhasil disimpan.');
    }

    public function show($cast_id)
    {
        $cast = Cast::find($cast_id);
        return view('show', compact('cast'));
    }

    public function edit($cast_id)
    {
        $cast = Cast::find($cast_id);
        return view('edit', compact('cast'));
    }

    public function update(Request $request, $cast_id)
    {
        $request->validate([
            'nama' => 'required|max:45',
        ]);

        $cast = Cast::find($cast_id);
        $cast->update($request->all());

        return redirect('/cast')->with('success', 'Data pemain film berhasil diperbarui.');
    }

    public function destroy($cast_id)
    {
        $cast = Cast::find($cast_id);
        $cast->delete();

        return redirect('/cast')->with('success', 'Data pemain film berhasil dihapus.');
    }
}
