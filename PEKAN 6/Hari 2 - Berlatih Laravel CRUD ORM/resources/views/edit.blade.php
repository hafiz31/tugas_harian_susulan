@extends('layout')

@section('content')
    <div class="container">
        <h2>Form Edit Data Pemain Film</h2>

        <!-- Form untuk mengedit data pemain film -->
        <form method="POST" action="{{ url("/cast/{$cast->id}") }}">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Nama:</label>
                <input class="form-control" type="text" name="nama" value="{{ $cast->nama }}" required>
            </div>
            <div class="form-group">
                <label for="umur">Umur:</label>
                <input class="form-control" type="number" name="umur" value="{{ $cast->umur }}" required>
            </div>
            <div class="form-group">
                <label for="bio">Bio:</label>
                <input class="form-control" type="text" name="bio" value="{{ $cast->bio }}" required>
            </div>
            <div class="row">
                <div class="col">
                    <a href="{{ url('/cast') }}"><-Kembali ke List</a>
                </div>
                <div class="col text-right">
                    <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                </div>
            </div>
        </form>
    </div>
@endsection
