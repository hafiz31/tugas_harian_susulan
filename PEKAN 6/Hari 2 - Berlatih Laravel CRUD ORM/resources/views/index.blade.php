@extends('layout')

@section('content')
    <div class="container my-2">
        <div class="row">
            <div class="col">
                <h2>List Data Para Pemain Film</h2>
            </div>
            <div class="col text-right">
                <a href="/cast/create" class="btn btn-primary">Tambah</a>
            </div>
        </div>

        <!-- Tampilkan data pemain film menggunakan tabel atau bootstrap card -->
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <!-- Loop through $casts to display data -->
                @foreach ($casts as $k => $cast)
                    <tr>
                        <td>{{ $k + 1 }}</td>
                        <td>{{ $cast->nama }}</td>
                        <td>{{ $cast->umur }}</td>
                        <td>{{ $cast->bio }}</td>
                        <td>
                            <a href="{{ url("/cast/{$cast->id}") }}" class="btn btn-primary btn-sm">Detail</a>
                            <a href="{{ url("/cast/{$cast->id}/edit") }}" class="btn btn-warning btn-sm">Edit</a>
                            <a href="{{ url("/cast/{$cast->id}/destroy") }}" class="btn btn-danger btn-sm">Delete</a>
                            <!-- You can add a delete button if needed -->
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
